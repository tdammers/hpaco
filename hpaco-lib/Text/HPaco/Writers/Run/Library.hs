module Text.HPaco.Writers.Run.Library
    ( loadLibrary
    )
where

import Data.Variant as V
import Data.Variant.ToFrom
import qualified Data.List.Split as Split
import Data.List as List
import Data.Tuple (swap)
import qualified System.Process as Process
import System.IO.Unsafe (unsafePerformIO)

join :: String -> [String] -> String
join sep lst = List.concat $ List.intersperse sep lst

split :: String -> String -> [String]
split sep str = Split.splitOn sep str

map_ :: [Variant] -> Variant
map_ (fv:xsv:_) =
    let f = case fv of
                Function ff -> (\a -> ff [a])
                otherwise -> id
        xs = V.values xsv
        ks = V.keys xsv
    in AList $ zip ks $ map f xs
map_ _ = V.Null

zip_ :: [Variant] -> Variant
zip_ (k:v:_) =
    let ka = values k
        kv = values v
    in AList $ zip ka kv
zip_ _ = V.Null

{-# NOINLINE pipe #-}
pipe :: [String] -> String -> String
pipe (cmd:args) stdin =
    unsafePerformIO $ Process.readProcess cmd args stdin

loadLibrary :: String -> Variant
loadLibrary "list" =
        AList [ ( String "count", Function (toVariant . length . toAList . head) )
              , ( String "sort", Function (toVariant . map swap . List.sort . map swap . toAList . head) )
              , ( String "zip", Function zip_ )
              ]
loadLibrary "string" =
        AList [ ( String "join", toVariant join )
              , ( String "split", toVariant split )
              ]
loadLibrary "os" =
        AList [ (String "pipe", toVariant pipe)
              ]
loadLibrary "std" =
        V.merge (loadLibrary "list") (loadLibrary "string")
loadLibrary "fp" =
        AList [ ( String "map", Function map_ )
              ]
loadLibrary other = Null
