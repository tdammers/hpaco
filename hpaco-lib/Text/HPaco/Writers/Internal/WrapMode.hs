module Text.HPaco.Writers.Internal.WrapMode
            ( WrapMode (..)
            )
where

data WrapMode = WrapNone | WrapFunction | WrapClass
    deriving (Eq)
