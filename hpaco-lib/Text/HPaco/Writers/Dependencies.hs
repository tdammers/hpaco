module Text.HPaco.Writers.Dependencies ( writeDependencies ) where

import Text.HPaco.AST.AST
import Data.List

writeDependencies :: String -> AST -> String
writeDependencies fn ast =
    fn ++ ": " ++ unwords (astDeps ast) ++ "\n"
