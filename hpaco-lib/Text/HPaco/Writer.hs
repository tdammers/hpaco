module Text.HPaco.Writer 
    ( Writer
    , WriterOptions (..)
    , defaultWriterOptions
    ) where

import Text.HPaco.AST (AST)
import Text.HPaco.Writers.Internal.WrapMode
import Text.HPaco.Writers.Internal.CodeWriter

data WriterError =
    WriterError { writerErrorMessage :: String }
    deriving (Show)
type Writer = AST -> String

data WriterOptions =
    WriterOptions { woPrettyPrint :: Bool
                  , woIndentStr :: String
                  , woTemplateName :: String
                  , woIncludePreamble :: Bool
                  , woWrapMode :: WrapMode
                  , woExposeAllFunctions :: Bool
                  , woWriteFunc :: String
                  , woSourcePositionComments :: Bool
                  }

defaultWriterOptions =
    WriterOptions { woPrettyPrint = False
                  , woIndentStr = "\t"
                  , woTemplateName = ""
                  , woIncludePreamble = True
                  , woWrapMode = WrapNone
                  , woExposeAllFunctions = False
                  , woWriteFunc = ""
                  , woSourcePositionComments = False
                  }

instance CodeWriterOptions WriterOptions where
    cwoIndent = woIndentStr
    cwoNewline = const "\n"
