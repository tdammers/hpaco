module Text.HPaco.Reader (Reader)
where

import Text.HPaco.AST (AST)

type SourceName = String
type Reader = SourceName -> String -> IO AST
