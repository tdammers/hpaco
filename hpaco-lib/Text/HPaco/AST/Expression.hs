module Text.HPaco.AST.Expression
where

data Expression = StringLiteral String
                | BooleanLiteral Bool
                | IntLiteral Integer
                | FloatLiteral Double
                | ListExpression [Expression]
                | AListExpression [(Expression,Expression)]
                | VariableReference String
                | EscapeExpression EscapeMode Expression
                | TernaryExpression Expression Expression Expression
                | BinaryExpression BinaryOperator Expression Expression
                | UnaryExpression UnaryOperator Expression
                | FunctionCallExpression Expression [Expression]
                deriving (Show, Eq)

data EscapeMode = EscapeHTML
                | EscapeURL
                deriving (Show, Eq)

data UnaryOperator = OpNot
                   deriving (Show, Eq)

data BinaryOperator = OpEquals
                    | OpNotEquals
                    | OpLooseEquals
                    | OpLooseNotEquals
                    | OpGreater
                    | OpLess
                    | OpNotGreater
                    | OpNotLess
                    | OpPlus
                    | OpMinus
                    | OpMul
                    | OpDiv
                    | OpMod
                    | OpMember
                    | OpBooleanAnd
                    | OpBooleanOr
                    | OpBooleanXor
                    | OpInList
                    | OpConcat
                    | OpCoalesce
                    | Flipped BinaryOperator
                    deriving (Show, Eq)
