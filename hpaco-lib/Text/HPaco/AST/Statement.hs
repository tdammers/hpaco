module Text.HPaco.AST.Statement where

import Text.HPaco.AST.Expression (Expression)
import Text.HPaco.AST.Identifier (Identifier)

data Statement = NullStatement
               | SourcePositionStatement FilePath Int
               | StatementSequence [Statement]
               | PrintStatement Expression
               | IfStatement Expression Statement Statement
               | LetStatement Identifier Expression Statement
               | ForStatement (Maybe Identifier) Identifier Expression Statement
               | CallStatement Identifier
               | SwitchStatement Expression [(Expression, Statement)]
               deriving (Show, Eq)
