HPaco - User's Guide
====================

Introduction
------------

Paco is a templating language. Its primary purpose is to provide a no-nonsense,
straightforward syntax for templates in PHP web applications. It goes back to a
very simple ad-hoc search-and-replace system that supported little more than
variable interpolation; the current version supports all sorts of programming
constructs, including macros, includes, a full expression parser, lexical
scoping, etc. etc.

HPaco is a compiler for the Paco template language, written in Haskell. It
takes input in Paco, and outputs PHP files that can be included directly into
an existing web application, or run as standalone pages.

Design Goals
------------
* **Simple and intuitive syntax**. Paco's syntax was designed to behave as one
  would expect, with a slight bias toward PHP programming.
* **Secure by default**. Anything you insert into Paco templates dynamically is
  automatically HTML-encoded by default; this means that it is hard to
  accidentally produce XSS vulnerabilities in your templates, since you won't
  have to remember to call `htmlspecialchars()` on anything. However, because
  sometimes you *do* know better, Paco allows you to override this behavior to
  do dangerous things.
* **Don't try to be smart.** Paco does not know about HTML or XML, but
  treats them as text. This means that you can put Paco constructs at any point
  in your HTML, and even produce invalid HTML - Paco doesn't care, its syntax
  will still work. This also means that while Paco's main goal is to produce
  HTML, it can also be used to output any other kind of (textual) data.

The `hpaco` Program
-------------------

`hpaco` is the compiler that transforms paco sources into a 'host language'. 
Currently two host languages are supported: PHP and JavaScript.

### JavaScript output options

#### `--js`

Enable JavaScript output.

#### `--js-pretty`

Pretty-prints the JavaScript code it outputs, so that it is somewhat
human-readable. For production use, this option should be 'on', because it
includes less unnecessary whitespace, making for somewhat smaller .js files.

#### `--js-raw`

Outputs raw JavaScript without any wrapper. The generated javascript is of the
form:

    (function(){
        // this is where the actual template code goes
    }).apply(context);

This means that at the point where the template gets included, a variable named
`context` should be in scope and point to the data to be passed into the
template.

#### `--js-func`

Outputs JavaScript wrapped in a function. The code will look something like
this:

    var runTemplate_templateName = function(context){
        // this is where the actual template code goes
    };

### PHP output options

#### `--php`

Enable PHP output. This option is the default.

#### `--php-pretty`

Pretty-prints the PHP code it outputs, so that it is somewhat human-readable.

#### `--php-raw`

Outputs raw PHP without any wrapper. To use such a template, either call it
directly (in which case it will function as a standalone page), or include it
right where you want it to produce output. You can feed it parameters by
setting a variable named `$context` in the containing scope.

#### `--php-func`

Outputs a function definition of the name `runTemplate_{template-name}` (or
`runTemplate` if your template doesn't have a name). Template names are derived
from the source filename, so `foobar.tpl` creates a function
`runTemplate_foobar`. The first (and only) parameter to this function is an
associative array or object that represents the template's initial scope. The
function runs the template and echoes the result.

#### `--php-class`

Outputs a class definition for a class named `Template_{template-name}` (or
just `Template` if your template doesn't have a name). The class implements an
`__invoke()` method, which runs the template and echoes the result.

In order to work properly, Paco needs to define a few functions; these function
definitions together are called the *'Preamble'*. HPaco includes a preamble,
with suitable guards to prevent multiple definitions, at the beginning of each
template, unless you specify `--php-no-preamble`. If you want to override any
of the default preamble functions, you can just define them before including
the template.
