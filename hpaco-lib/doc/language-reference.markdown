Paco Language Reference
=======================

General
-------

Paco's building blocks are **statements** and **expressions**. At the top
level, a Paco template consists of statements, and depending on their type,
these can contain other statements as well as expressions.

Conceptually, statements may produce output, but not return values; expressions
return values but should not produce any output (although depending on the
functions you bind into Paco from the host environment, they *can* - it's just
that they *shouldn't*, but there is nothing in Paco to stop them).

Capo
----
There is an alternative syntax for Paco templates called Capo; it aims to be a
more suitable for logic-heavy templates than Paco, sacrificing a typical
template-like syntax. Capo shares its expression syntax with Paco, but the
statement syntax is different, resembling typical curly-brace languages like C,
JavaScript or PHP.

Statements
----------

### Direct output (Paco only)

Any text outside curly braces (`{}`) is considered direct output and will be
left completely unchanged. This means that any HTML document that does not
contain any curly braces is automatically also valid Paco.

### Interpolation (Paco only)

Expressions between curly braces (`{}`) are interpolated into the output where
they appear. The most simple expressions are literals and variables, but they
can become as complicated as you like.

By default, interpolation applies HTML-encoding to protect from XSS
vulnerabilities, so for example `{'A & B'}` renders as `A &amp; B`. You can
override this behavior using an encoding specifier directly after the opening
curly brace. Valid encoding specifiers are `!` (no encoding at all) and `@`
(apply URL-encoding instead of HTML-encoding). So, for example, `{!'A & B'}`
renders as `A & B`, `{@'A & B'}` produces `A%20%26%20B`.

### Print statements (Capo only)

To generate any output from Capo, a call to the `print` pseudofunction is
required. The syntax is:

    print(expression);

### Conditionals: The `if` statement

If statements come in two flavors, with and without an 'else' branch. The
syntax is:

    {%if condition%}
        (true-statements)
    {%else%}
        (false-statements)
    {%endif%}

or, without the 'else' branch:

    {%if condition%}
        (true-statements)
    {%endif%}

`condition` can be any expression; when it evaluates to a falsy value, the
'`false-statements`' branch is evaluated, otherwise the '`true-statements`'.

The equivalent Capo syntax:

    if (condition) {
      true-statements;
    }
    else {
      false-statements;
    }

or just:

    if (condition) {
      true-statements;
    }

**A NOTE ON FALSINESS**

Most dynamically-typed programming languages have a notion of 'falsiness',
which usually means that a falsy value is one that causes a condition to be
interpreted as 'false'. Boolean `false`, `null`, and numeric zero (`0`) are
typical falsy values, but there are edge cases where programming languages
differ. Since Paco relies on the falsiness rules of the underlying output
language, it is recommended that you only test values for falsiness that are
'safe', that is `0`, `false` and `null`, or their non-falsy counterparts.

### Loops: The `for` statement

The only way to loop in Paco is through the `for` construct, which iterates
over a list, map, or (depending on the host language) other iterable structure.
The syntax comes in three flavors:

    {%for iterable%}
        (body-statements)
    {%endfor%}

and

    {%for iterable : i%}
        (body-statements)
    {%endfor%}

and

    {%for iterable : key i%}
        (body-statements)
    {%endfor%}

or, in Capo:

    for (iterable) {
        body-statements;
    }

and

    for (iterable as i) {
        body-statements;
    }

and

    for (iterable as key => i) {
        body-statements;
    }

In Capo, a `:` character can be used instead of the `as` keyword, and `->` can
be used instead of `=>`.

In the first flavor, each element of the `iterable` is visited, and the
body-statements are executed for each of them; inside the body, the iterated
element supersedes the current scope, hiding variables from the containing 
scope. The second flavor explicitly maps the iterated element to a local
variable, retaining all other variables from the containing scope.

The third flavor allows iterating over key-value lists, pulling not only the 
values into the body's scope, but also the keys. There are two syntax 
variations on this flavor:

    {%for iterable : key -> i %}

and

    {%for iterable : i <- key %}

There is a shorthand for nested loops; the following two code samples are
equivalent:

    {%for foo : bar %}
        {%for baz : quux %}
            (body)
        {%endfor%}
    {%endfor%}

    {%for foo : bar, baz : quux %}
        (body)
    {%endfor%}

in Capo:

    for (foo : bar, baz : quux) {
        body;
    }

### Scope: The `with` statement

The `with` statement introduces a local scope, which can reduce typing effort,
as well as prepare a context for shared code (see 'Macros'). The syntax, just
like with `for` statements, comes in two flavors:

    {%with expression%}
        (body-statements)
    {%endwith%}

and

    {%with expression : i%}
        (body-statements)
    {%endwith%}

In Capo:

    with (expression) {
        body-statements;
    }

and

    with (i = expression) {
        body-statements;
    }

The local scope works just like the scope introduced by `for` statements, only
that instead of looping and taking elements, the `with` statement executes its
body only once and uses the argument directly, instead of its elements.

Just like `for` statements, nested `with` statements have a shorthand:

    {%with foo : bar, baz : quux %}
    {%endwith%}

in Capo:

    with (bar = foo, quux = baz) {
    }

Capo provides another bit of syntax sugar for `with` statements, in the form of
an implicit `with` block. The following two examples are equivalent:

*Explicit `with`:*

    with (x = foobar) {
        print(x);
    }

*Implicit `with`:*

    x = foobar;
    print(x);

**CAUTION:**

Implicit `with` introduces an implicit scoping block, and while it looks like
an assignment in an imperative language with destructive update, it is really
not, because Capo is still a mostly-pure language. The original variable under
the same name, if any, is preserved, and the new one merely shadows it. As soon
as the containing scope exits, the original variable is restored.

One example where this can be quite counter-intuitive is inside loops:

    i = 1;
    for (["foo", "bar", "baz"] : elem) [
        print(i, ": ", elem, "; ");
        i = i + 1;
    }

Intuition gained from a language like C says this should print "1: foo; 2: bar;
3: baz; ", but instead, it prints "1: foo; 1: bar; 1: baz; ". The reason is
obvious when we desugar this code:

    with (i = 1) {
        for (["foo", "bar", "baz"] : elem) [
            print(i, ": ", elem, "; ");
            with (i = i + 1) {
            }
        }
    }

What happens is that at the end of the `for` body, a new scope is introduced in
which the variable i equals the value of i from the containing scope plus one;
but then the `for` body ends, and the scope pops back.

### Macros: The `def` and `call` statements

The `def` statement creates a macro with a name. The syntax is as follows:

    {%def macro_name%}
        (body-statements)
    {%enddef%}

You can call a macro with the current scope like this:

    {%call macro_name%}

Or you can give it a different scope, like this:

    {%call macro_name with scope_expression%}

...or you can bind an expression to a local variable for the scope of the call:

    {%call macro_name with expression : local_variable_name%}

The above are equivalent to:

    {%with scope_expression%}{%call macro_name%}{%endwith%}

and

    {%with expression : local_variable_name%}{%call macro_name%}{%endwith%}

### Includes

Using the `include` statement, you can pull Paco source code from other files
into the current template.

    {%include filename%}

Included files are always searched relative to the current file; if the include
directive does not have an extension, the current file's extension is
substituted. For example, if you write the following in the file
`~/templates/main.tpl`:

    {%include includes/inner%}

...then the file to be included must be at `~/templates/includes/inner.tpl`.

Expressions
-----------

Paco has a rich expression syntax, including the following constructs.

### Literals

**String literals** can be enclosed in single or double quotes. Both are
equivalent in their meaning. *Example:* `{'This is a string'}` `{"This is a
string"}`.

**Numeric literals** currently (version 0.6) HPaco only supports integer
literals; decimal numbers will be supported in the future. Example: `{1423}`.

### Variables

**Variable references** consist of just the variable name. Example: `{var}`.

**Member access**: variables that contain complex values such as associative
arrays or objects, can be dereferenced like this: `{some_object.member}`.

**Index access**: you can also descend into complex variables using array
syntax: `{some_object['member']}`. Array syntax is much more flexible than
object-style member access syntax, because it allows for arbitrary expressions
to be used as the index, while object-style syntax can only access members
whose names are valid identifiers (alphanumeric characters and underscores
only).

### Lists and maps

It is possible to construct complex values from other values. There are two
basic data structures for this in Paco: *lists* and *maps*. These correspond to
lists and objects in JSON, and the syntax is very similar.

**List construct**. A list is a flat list of values, with implicit 0-based
indexes: `[ elem0, elem1, elem2, elem3, ... elemn ]`. For convenience, a comma
is allowed between the last element and the closing square bracket. Each
element can be any type of expression, including other lists and maps.

**Map construct**. A map is a key-value data structure, specified in pairs of
keys and values. Keys must be unique, and their order in the map is
unspecified. While it is possible to iterate over a map, the order in which
elements are visited is unspecified, which is why it is discouraged. The map
syntax is: `{ key0 : value0, key1 : value1, ... keyn : valuen }`. Just like
with lists, an extra comma is allowed between the last value and the closing
curly brace. Keys and values can be arbitrary expressions, but the keys must
evaluate to a primitive value (integer, string, boolean) at run-time.

### Function calls

It is possible to map functions to context variables, and if you do that, you
can call those functions from within Paco. There are two syntax flavors for
this:

**Old-style function call**: `$function arg1 arg2 ...`

**New-style function call**: `function(arg1, arg2, ...)`

Old-style function call syntax is included for compatibility with previous
implementations; it is recommended that you use new-style function call syntax.

There is one special function call: the `library` pseudo-function loads a
built-in function library and returns it as a map. See the section 'libraries'
below for details. The `library` pseudo-function is most useful in a `{%with%}`
construct, e.g.:

    {%--
        import functions from the 'std' library into the current scope:
      --%}
    {%with library('std')%}
        {count(items)}
    {%endwith%}

    {%--
        import functions from the 'std' library into a variable named 'std':
      --%}
    {%with library('std') : std%}
        {std.count(items)}
    {%endwith%}

    {%--
        alternative form of the above:
      --%}
    {%with {'std':library('std')} %}
        {std.count(items)}
    {%endwith%}

### Binary operators

The following binary (infix) operators are available, in order of precedence:

* **Boolean expressions**: `&&` (logical AND), `||` (logical OR), `^^` (logical
  XOR).
* **Set operations**: `in` (right operand contains left operand), `contains`
  (left operand contains right operand); `a in b` is equivalent to `b contains
  a`.
* **Comparisons**: `<`, `>`, `<=`, `>=`, `=`, `!=`, `==`, `!==`. These all do
  what you'd expect; `=` and `!=` perform non-strict comparisons, `==` and
  `!==` perform strict comparisons. Just like in PHP and Javascript, strict
  comparison means that two values match only if their types are also the same.
* **Additive**: `+` and `-`. Perform numeric addition and subtraction.
* **Multiplicative**: `*` and `/` for multiplication and division; `%` does
  integer modulo.

### Unary operators

Paco currently has one unary operator, logical NOT (`not`).

## Libraries

To use a library, use the built-in function `library`, which takes a library
name as its only parameter and returns an object containing the library
functions. Typical usage looks something like this:

{%with library('std')%}
{count(foobar)}
{%endwith%}

Libraries are a special feature of the `hpaco` implementation.

### `std`: Various common operations

The `std` library is a convenience library that includes the `string` and
`list` libraries. Loading `std` is equivalent to loading `

### `string`: String manipulation

* `join(`_sep_`, `_items_`)`: concatenates elements of _items_ into a single
  string, inserting the separator _sep_ between each pair. **Example:**
  `{std.join("-", [1,2,3])}` outputs `1-2-3`.
* `split(`_sep_`, `_str_`)`: splits the string _str_ at each occurrence of the
  separator _sep_.

### `list`: List / array manipulation

* `count(`_item_`)`: returns the number of elements in _item_, which should be
  an array. **Example:** `{std.count([1,2,3])}` outputs `3`.
* `sort(`_arr_`)`: sorts the array _arr_, returning a sorted copy.
* `zip(`_keys_`, `_values_`)`: creates a key/value list, using keys from the
  _keys_ array and values from the _values_ array.

### `fp`: Functional-programming helpers

* `map(`_function_`, `_items_`)`: iterate over _items_, applying _function_ to
  each element and returning a list of return values.
