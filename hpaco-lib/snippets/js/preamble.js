	var _clone = function (obj){
		if(obj == null || typeof(obj) != 'object')
			return obj;
		var temp = obj.constructor();
		for(var key in obj)
			temp[key] = _clone(obj[key]);
		return temp;
	}
	var _merge = function(p,c) {
		if(c == null || typeof(c) != 'object')
			return c;
		var res = _clone(p);
		for(var key in c)
			res[key] = _clone(c[key]);
		return res;
	}

	var _htmlencode = function(str){
		return String(str)
			.replace(/&/g, '&amp;')
			.replace(/"/g, '&quot;')
			.replace(/'/g, '&#39;')
			.replace(/</g, '&lt;')
			.replace(/>/g, '&gt;');
	};
	var _f = function(str){
		if (Array.isArray(str)) {
			var res = '';
			for (var i = 0; i < str.length; ++i) { if (i > 0) { res += ' '; } res += _f(str[i]); }
			return res;
		}
		return String(str);
	};
	var _a = function(a) {
		if (Array.isArray(a)) return a;
		if (typeof(a) === 'object') {
			var aa = [];
			for (var p in a) {
				aa.push(a[p]);
			}
		}
		return [];
	}
	var _in = function(a,b) {
		if (!Array.isArray(b)){return false;}
		for (var i = 0; i < b.length; ++i) { if (a == b[i]) return true; }
		return false;
	};
	var _loadlib = function (libname) {
		switch (libname) {
			case 'list':
				return {
					'count' : function(a){return _a(a).length;},
					'sort' : function(a){return _a(a).sort();},
					'zip' : function(k,v){
								var ka = _a(k);
								var va = _a(v);
								var o = {};
								for (var i = 0; i < ka.length && i < va.length; ++i) {
									o[ka[i]] = va[i];
								}
								return o;
							}
				};
			case 'string':
				return {
					'join' : function(s,a){return _a(a).join(s);},
					'split' : function(s,a){return _f(a).split(s);}
				};
			case 'std':
				return _merge(_loadlib('string'), _loadlib('list'));
			case 'fp':
				return {
					'map' : function(f,a){
						if (Array.isArray(a)) {
							var o = [];
							for (var i = 0; i < a.length; ++i) {
								o.push(f(a[i]));
							}
							return o;
						}
						if (typeof(a) === 'object') {
							var o = {};
							for(p in a) {
								o[p] = f(a[p]);
							}
							return o;
						}
						return null;
					}
				};
			default: throw new Exception('No such library: ' + libname);
		}
	}
	var _scope = context;
	var _iteree = null;
	var _index = null;
