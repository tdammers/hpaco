<?php
if (!class_exists('_S', false)) {
	class _S {
		public $t;
		public $p;
		public function __construct($t, $p) { $this->t = $t; $this->p = $p; }
		public function __isset($key) {
			return (
				(is_array($this->t) && isset($this->t[$key])) ||
				(is_object($this->t) && isset($this->t->$key)) ||
				(is_array($this->p) && isset($this->p[$key])) ||
				(is_object($this->p) && isset($this->p->$key)));
		}
		public function __get($key) {
			if (is_array($this->t) && isset($this->t[$key])) return $this->t[$key];
			if (is_object($this->t) && isset($this->t->$key)) return $this->t->$key;
			if (is_array($this->p) && isset($this->p[$key])) return $this->p[$key];
			if (is_object($this->p) && isset($this->p->$key)) return $this->p->$key;
			return null;
		}
	}
}
if (!class_exists('_LF', false)) {
	class _LF {
		public $f;
		public function __construct($f) { $this->f = $f; }
		public function __invoke() { return call_user_func_array($this->f, func_get_args()); }
		public function __toString() { return '<<function>>'; }
	}
}
if (!class_exists('_AF', false)) {
	class _AF {
		public function __isset($key) {
			return is_callable($key);
		}
		public function __get($key) {
			if (is_callable($key))
				return new _LF($key);
			else
				return null;
		}
	}
}
if (!is_callable('_r')) {
	function _r($context, $key) {
		if ($key === '.') return $context;
		if ($key === 'library') return new _LL;
		if (is_array($context)) {
			if (isset($context[$key])) return $context[$key];
		}
		if (is_object($context)) {
			if (isset($context->$key)) return $context->$key;
		}
		return null;
	}
}
if (!is_callable('_in')) {
	function _in($elem, $list) {
		if (is_array($list)) {
			return in_array($elem, $list);
		}
		return false;
	}
}
if (!is_callable('_f')) {
	function _f($val) {
		if (is_array($val)) {
			return implode(' ', $val);
		}
		return (string)$val;
	}
}
if (!class_exists('_LL', false)) {
	class _LL {
		 
		public function __invoke($libname) {
			switch ($libname) {
				case 'std':
					return array_merge($this->__invoke('string'), $this->__invoke('list'));
				case 'list':
					return array(
						'count' => new _LF('count'),
						'sort' => new _LF(function($a){sort($a);return $a;}),
						'zip' => new _LF('array_combine'),
					);
				case 'string':
					return array(
						'join' => new _LF(function($sep,$arr){if(!is_array($arr)) return array();return implode(_f($sep),$arr);}),
						'split' => new _LF(function($sep, $str){return explode(_f($sep),_f($str));}),
					);
				case 'fp':
					return array(
						'map' => new _LF(function($f,$a){
												return array_map(
													function($x) use ($f) {return _call($f,array($x));},
													$a);
											}),
						);
				case 'php.all_functions':
					return new _AF();
				default: throw new Exception('No such library: ' . $libname);
			}
		}
	}
}
if (!is_callable('_call')) {
	function _call($func, $args) {
		if ($func instanceof FunctionPointer || $func instanceof _LL || $func instanceof _LF) {
			return call_user_func_array($func, $args);
		}
		return null;
	}
}
